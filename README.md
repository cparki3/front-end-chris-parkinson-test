# Front-end Developer Exercise #

1. Fork this repository
1. Build single webpage based on the [comp](https://invis.io/RCBBKNG9G)
1. Create a pull request to this repository

### Success Criteria ###
* Mobile First Fully Responsive
* Minimally compatible with:
    * Internet Explorer 10
    * Chrome 30
    * Safari 6.1
    * Firefox 30
* Markup, styles, scripts, and project structure should be well organized and thoughtful
* Project should match comps
* Project should run in any required browser

### Notes ###
* We use HTML, CSS and JS. Show us you can too!
* Leverage any library or build tool you like, but have a good reason for it.

### Resources ###

The visual design team has provided several deliverables to help with construction of the page:

* All of the copy can be found at ./resources/body-copy.txt.

* Any image you need can be found in the ./assets

* Comps are built in Sketch and can be found in ./resources/rogue-front-end-test.sketch

* Video Links:
    * https://www.youtube.com/watch?v=bDmSL31nnfo
    * https://www.youtube.com/watch?v=pGnszxqCU5s
    * https://www.youtube.com/watch?v=CGPfgz58JtA


* Font used: [Roboto](https://fonts.google.com/specimen/Roboto)


### Final Thoughts ###
We're developers. We like building cool stuff and have fun doing it; so should you! Relax, and have some fun with this.

**Rogue Frontend - Move Fast and Break Things**

### Chris Parkinson Summary ###
I implemented a couple of small tools to help with development. One was "skeleton.css" which is a light weight css framework that basically provides a nice column/row workflow. I also used a small lightbox component created with jQuery for the gallery and image setups. I didn't have to do a ton of javascript but I can certainly write my own lightbox if that's something you guys want to see. The biggest chunk of javascript I had to write was to properly position that map when the screen size changes (dang that's a good test lol).

At Discover we don't do a lot of mobile work so this was a nice change of pace. I only used one media query to check if the width was over 768 but if I had some more time to play with this I would add in some extra queries.

Thanks a lot for the opportunity and please let me know if I missed anything or what I can do to match your workflow.  
