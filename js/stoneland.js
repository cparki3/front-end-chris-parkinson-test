$(document).ready(function(){
    $('.venobox').venobox();
    checkMapPosition();
});

$(window).resize(function(){
  checkMapPosition();
});

function showGallery()
{
  //USES VENOBOX WHEN GALLERY BUTTON IS CLICKED
  $("#galleryStart").venobox().trigger('click');
}

function startVideo()
{
  //USES VENOBOX WHEN VIDEO BUTTON IS CLICKED
  $("#videoStart").venobox().trigger('click');
}

function checkMapPosition()
{
  // A LOT OF CHECKS TO MAKE SURE THE MAP STAYS IN THE CORRECT SPOT AND SCALES CORRECTLY
  var locationOffset = $("#locationText").offset().top;
  var glasgowOffset = $("#glasgow").offset().top;
  var firstPinOffset = $("#pin-1").offset().top;
  var introHeight = $("#location-intro").innerHeight();
  var mapImageTop = $("#map-image").offset().top;
  var mapTop = (mapImageTop - firstPinOffset) + introHeight + 30;
  var mapHeight = (glasgowOffset - firstPinOffset) + introHeight;
  $("#map").css({top: mapTop + "px"});
  $(".map-space").height(mapHeight);
}
